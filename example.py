#!/usr/bin/env python3
from decimal import Decimal, ROUND_HALF_UP

from rebalance import Commodity, Holder, Account, D, Holder
from rebalance import distribute, get_total_for, parse_commodity
from rebalance import EUR, RUB, USD


def main():
    print("This is a sample savings portfolio")

    Holder1 = Holder("Holder1")
    Holder2 = Holder("Holder2")

    # Metals
    XAU = Commodity('XAU', RUB(Decimal("4657.57")))
    XAG = Commodity('XAG', RUB(Decimal("57.43")))

    # Stocks
    AAPL = Commodity('AAPL', Decimal('118.97'))
    TSLA = Commodity('TSLA', Decimal('438.55'))
    FB = Commodity('FB', Decimal("265.15"))
    GAZP = Commodity('GAZP', RUB(Decimal('163.98')))

    # Stocks funds
    AKSP = Commodity('AKSP')  # Price taken from MOEX
    FXUS = Commodity('FXUS', RUB(Decimal(4788)))
    VTBA = Commodity('VTBA', Decimal('11.70'))

    # Bonds funds
    VTBU = Commodity('VTBU')  # Price taken from MOEX
    VTBH = Commodity('VTBH', Decimal("10.26"))
    FXTB = Commodity('FXTB', RUB(Decimal("790.50")))
    SBGB = Commodity('SBGB', RUB(Decimal("1233.00")))

    stocks = D({
        D({
            Account("AAPL", Holder1, 'stocks', AAPL): 1,
            Account("TSLA", Holder1, 'stocks', TSLA): 1,
            Account("FB", Holder1, 'stocks', FB): 1,
            Account("GAZP", Holder2, 'stocks', GAZP): 3,
        }): 1,
        D({
            Account('FXUS', Holder2, 'stocks', FXUS): 1,
            Account('AKSP', Holder1, 'stocks', AKSP): 1,
            Account('VTBA', Holder1, 'stocks', VTBA): 1,
        }): 4,
    })

    bonds = D({
        Account('VTBU', Holder1, 'bonds', VTBU): 1, # Корп еврооблигации РФ
        Account('VTBH', Holder1, 'bonds', VTBH): 1, # Корп еврооблигации США
        Account('FXTB', Holder1, 'bonds', FXTB): 1, # Казначеийские облигации США
        Account('SBGB', Holder2, 'bonds', SBGB): 1, # Индекс ОФЗ
    })

    metals = D({
        Account('Gold', Holder1, 'metal', XAU): 1,
        Account('Silver', Holder2, 'metal', XAG, quant=100): 1,
    })

    cash = D({
        Account('EUR', Holder1, 'deposit', EUR): 1,
        Account('USD', Holder2, 'deposit', USD): 1,
    })

    distribution = {
        stocks: 1,
        bonds: 1,
        metals: 1,
        cash: 1,
    }

    total = get_total_for(['bonds', 'stocks', 'metals', 'deposit'])
    print(f"Total savings: ${total}")

    result = total - distribute(total, distribution)

    print(f"Rounding error: ${result}")
    print("By holder:")
    for holder in (Holder1, Holder2):
        percent = (holder.result*100 / total).quantize(Decimal('0.1'), ROUND_HALF_UP)
        print(f'{holder.name:} ${holder.result} {percent}%')


if __name__ == '__main__':
    main()
