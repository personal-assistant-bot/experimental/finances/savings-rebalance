import subprocess

from decimal import Decimal, ROUND_HALF_UP
from functools import partial

import moex

from formatter import recommend


class Commodity:

    def __init__(self, ticker, price=None, quant=1):
        self._ticker = ticker
        if price is None:
            self._price = moex.get_price(ticker)
        else:
            self._price = price
        self._quant = Decimal(f"{quant:e}").normalize()

    @property
    def ticker(self):
        return self._ticker

    @property
    def quant(self):
        return self._quant

    def _quantize(self, value):
        return value.quantize(self._quant)

    def buy(self, to_spend, quantize=None):
        quant_func = quantize or self._quantize
        amount = quant_func(to_spend / self._price)
        return amount, (amount * self._price).quantize(Decimal('1.00'))

    def __call__(self, amount):
        return self._quantize(self._price * amount)

    def __str__(self):
        return self._ticker


class Currency(Commodity):

    def __init__(self, name, price=None, ticker=None, quant=0.01):
        if ticker is not None:
            price = moex.get_price(ticker)
        super().__init__(name, price, quant)


USDRUB = Decimal('78.78')

USD = Currency('$', 1)
EUR = Currency('EUR', ticker="EURUSD")
RUB = Currency('', 1/USDRUB)


def parse_commodity(s):
    if s[0] == '$':
        return Decimal(s[1:])
    parts = s.split()
    if len(parts) == 1:
        return RUB(Decimal(s))

    if parts[0] == 'EUR':
        return EUR(Decimal(parts[1]))

    raise Exception(f"Won't parse commodity {s}")


class cached_property(object):

    def __init__(self, func):
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self
        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value


class Holder:

    def __init__(self, name):
        self._name = name
        self._result = 0

    @property
    def name(self):
        return self._name

    def put(self, amount):
        self._result += amount

    @property
    def result(self):
        return self._result


class Account:

    def __init__(self, name, holder, query, commodity, quant=None, minimum=None):
        self._name = name
        self._commodity = commodity
        self._query = query
        self._quant = Decimal(f"{quant:e}").normalize() if quant else None
        self._holder = holder
        self._min = minimum

    @property
    def name(self):
        return self._name

    @cached_property
    def amount(self):
        cur = self._commodity.ticker
        if cur == '$':
            cur = '\$'
        output = subprocess.run(["hledger", "bal", self._query,
                                 f"cur:{cur}",
                                 "--flat", "--format", "%(total)", "-N"],
                                stdout=subprocess.PIPE)
        total = Decimal(0)
        for line in output.stdout.decode().split('\n'):
            line = line.strip()
            if not line:
                continue
            if line[0] == '$':
                total += Decimal(line[1:])
            else:
                s = line.split()
                if s[0].isdigit():
                    total += Decimal(s[0])
                else:
                    total += Decimal(s[1])
        return total

    def _quantize(self, amount):
        a = amount.quantize(self._quant, ROUND_HALF_UP)
        return a.quantize(self._commodity.quant)

    def __call__(self, to_spend):
        if self._min is not None and to_spend < self._min:
            new, spent = 0, Decimal()
        elif self._quant is not None:
            new, spent = self._commodity.buy(to_spend, self._quantize)
        else:
            new, spent = self._commodity.buy(to_spend)
        advice = recommend(self.amount, new, self._name, self._holder, spent)
        if advice is not None:
            print(advice)
        self._holder.put(spent)
        return spent


def get_total_for(accts):
    output = subprocess.run(["hledger", "bal"]
                            + accts
                            + ["--value=end,$", "--flat",
                               "--format", "%(total)", "-N"],
                            stdout=subprocess.PIPE)
    total = Decimal(0)
    for line in output.stdout.decode().split('\n'):
        line = line.strip()
        if not line:
            continue
        total += Decimal(line[1:])
    return total


def get_count(currency):
    output = subprocess.run(["hledger", "bal", "^assets", f"cur:{currency}",
                             "--flat", "--format", "%(total)", "-N"],
                            stdout=subprocess.PIPE)
    total = Decimal(0)
    for line in output.stdout.decode().split('\n'):
        line = line.strip()
        if not line:
            continue
        if line[0] == '$':
            total += Decimal(line[1:])
        else:
            total += Decimal(line.split()[0])
    return total


def distribute(initial_amount, distribution):
    amount = initial_amount
    total_weight = sum(distribution.values())
    for key, weight in distribution.items():
        if total_weight > 0:
            to_spend = amount * Decimal(weight / total_weight)
        else:
            to_spend = Decimal(0)
        spent = key(to_spend.quantize(Decimal('1.00')))
        amount -= spent
        total_weight -= weight
    return initial_amount - amount


D = lambda d: partial(distribute, distribution=d)
