from decimal import Decimal
from typing import NamedTuple

_action = {
    'buy': ('Buy', '+'),
    'buy!': ('Buy', '+'),
    'sell': ('Sell', '-'),
    'sell!': ('Sell all', '-'),
}

_maybe = {
    'buy': ('Maybe buy', '?'),
    'sell': ('Maybe sell', '?'),
    'keep': ('Keep', ' '),
}

class AssetChange(NamedTuple):
    holder: str
    amount: Decimal
    result: Decimal
    asset: str
    change: Decimal
    cost: Decimal


class Advice:
    def __init__(self, data: AssetChange, action: str):
        self._data = data

        if abs(self._data.change) < 10:
            name, symbol = _maybe[action]
        else:
            name, symbol = _action[action]

        self.head = f"{symbol} {self._data.holder}: {name}"
        self.tail = f"for ${data.cost:.2f}"

        self.result = {
            'buy!': self.buy_new,
            'sell!': self.sell_all,
            'buy': self.buy,
            'sell': self.sell,
            'keep': self.keep,
        }[action]()

    @property
    def change_up(self) -> str:
        return f"{self._data.amount} more {self._data.asset}, up to"

    @property
    def change_down(self) -> str:
        return f"{self._data.amount} {self._data.asset}, down to"

    @property
    def change_value(self) -> str:
        return f"{self._data.amount} {self._data.asset}"

    @property
    def new_value(self) -> str:
        return f"{self._data.result} {self._data.asset}"

    @property
    def change_by(self) -> str:
        return f"{self._data.result} ({self._data.change:+.2f}%)"

    def buy_new(self) -> [str]:
        return [self.change_value, self.tail]

    def sell_all(self) -> [str]:
        return [self.change_value, self.tail]

    def buy(self) -> [str]:
        return [self.change_up, self.change_by, self.tail]

    def sell(self) -> [str]:
        return [self.change_down, self.change_by, self.tail]

    def keep(self) -> [str]:
        return [self.new_value]

    def __str__(self):
        return ' '.join([self.head, *self.result])


def recommend(current, new, name, holder, value):
    amount = abs(new - current)

    if 0 in (current, new):
        change = 100
        cost = value
    else:
        change = (new - current)/current*100
        cost = value / new * amount

    data = AssetChange(holder.name, amount, new, name, change, cost)

    if new > current == 0:
        action = 'buy!'
    elif new > current:
        action = 'buy'
    elif new == current > 0:
        action = 'keep'
    elif current > new == 0:
        action = 'sell!'
    elif new < current:
        action = 'sell'
    else:
        assert new == current == 0
        return

    return Advice(data, action)
