#!/usr/bin/env python3
import decimal
import requests

URL = 'http://iss.moex.com/'

boards = {
    'TQTD': ('stock', 'shares'),
    'TQTF': ('stock', 'shares'),
    'FIXI': ('currency', 'index'),
    'FQBR': ('stock', 'foreignshares'),
}

ticker_fix = {
    'EURUSD': 'EURUSDFIX',
}


def search_board(board):
    raise Exception(f"Not implemented: search_board({board})")


def get_ticker_info(ticker):
    where = "iss/securities.json"
    url = f"{URL}{where}"
    response = requests.get(url, params={'q': ticker})
    data = response.json()['securities']
    for row in data['data']:
        item = {k: v for k, v in zip(data['columns'], row)}
        if item['secid'] == ticker:
            return item
    print(f"Ticker {ticker} not found at MOEX:")
    print(response.json())
    raise "Not found"


def get_price_from_info(info):
    board = info.get('primary_boardid')
    if board not in boards:
        search_board(board)
    engine, market = boards[board]
    secid = info.get('secid')

    where = f"iss/engines/{engine}/markets/{market}/boards/{board}/securities/{secid}.json"

    response = requests.get(URL+where, params={'iss.only': 'marketdata',
                                               'securities.columns': 'LAST,LASTVALUE'})
    data = response.json()['marketdata']
    item = {k: v for k, v in zip(data['columns'], data['data'][0])}
    for k in ('LAST', 'LASTVALUE'):
        if k in item and item[k] is not None:
            return decimal.Decimal(str(item[k]))
    if 'BID' in item and 'OFFER' in item and item['BID'] is not None:
        bid = decimal.Decimal(str(item['BID']))
        ask = decimal.Decimal(str(item['OFFER']))
        return (bid + ask) / 2
    print(f"Can't parse:")
    print(response.json())
    print(item)
    raise Exception("Not implemented")


def get_price(ticker):
    if ticker in ticker_fix:
        ticker = ticker_fix[ticker]
    info = get_ticker_info(ticker)
    result = get_price_from_info(info)
    print("  -- MOEX:", ticker, result)
    return result


if __name__ == '__main__':
    print(get_price('TUSD'))
